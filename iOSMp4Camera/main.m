//
//  main.m
//  iOSMp4Camera
//
//  Created by Boris Lapinski on 12-12-14.
//  Copyright (c) 2014 Boris Lapinski. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TXXAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TXXAppDelegate class]));
    }
}
