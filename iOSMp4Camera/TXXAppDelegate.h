//
//  TXXAppDelegate.h
//  iOSMp4Camera
//
//  Created by Boris Lapinski on 12-12-14.
//  Copyright (c) 2014 Boris Lapinski. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TXXViewController;

@interface TXXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) TXXViewController *viewController;

@end
